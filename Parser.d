import Lexer;


class Parser {
    Lexer _lexer;
    Token _currentToken;

    this(Lexer l) {
        _lexer = l;
        _currentToken = _lexer.getNextToken();
    }

    void eat(TokenType type) {
        assert(_currentToken.type == type);
        _currentToken = _lexer.getNextToken();
    }

    //Will be using same terminology as mathematical expressions.
    Node factor() {
        Token token = _currentToken;
        if(token.type == TokenType.NEGATION) {
            eat(TokenType.NEGATION);
            auto node = new UnNode(factor(), token); 
            return node;
        } else if (token.type == TokenType.PROPOSITION) {
            eat(TokenType.PROPOSITION);
            return new LeafNode(token);
        } else if ( token.type == TokenType.LPAREN) {
            eat(TokenType.LPAREN);
            auto node = expr();
            eat(TokenType.RPAREN);
            return node;
        }
        assert(false);
    }

    Node term() {
        auto node = factor();
        while (_currentToken.type == TokenType.IMPLY || 
               _currentToken.type == TokenType.EQUALS)  {
            Token token = _currentToken;
            if (token.type == TokenType.IMPLY) eat(TokenType.IMPLY);
            else if (token.type == TokenType.EQUALS) eat(TokenType.EQUALS);

            node = new BinNode(node, token, factor());
        } 
        return node;
    }

    Node expr() {
        auto node = term();
        while (_currentToken.type == TokenType.AND || 
               _currentToken.type == TokenType.OR)  {
            Token token = _currentToken;
            if (token.type == TokenType.AND) eat(TokenType.AND);
            else if (token.type == TokenType.OR) eat(TokenType.OR);

            node = new BinNode(node, token, term());
        } 
        return node;

    }

    Node parse() {
        return expr();
    }
}

/****************************************************
*                                                   *
*           NODE CLASSES FOR PARSE TREE             *
*                                                   *
*****************************************************/
enum NodeType {BINARY, UNARY, LEAF}

class Node {
    NodeType _nodeType;

    override string toString() const {
        return "Base node.";
    }
        
}

class BinNode : Node {
    Node _left;
    Token _operator;
    Node _right;

    this(Node l, Token op, Node r) {
        _left = l;
        _operator = op;
        _right = r;
        _nodeType = NodeType.BINARY;
    }

    override string toString() const {
        return "Binary operator node." ~ _operator.toString();
    }
}

class UnNode : Node {
    Node _next;
    Token _operator;

    this(Node n, Token op) {
        _next = n;
        _operator = op;
        _nodeType = NodeType.UNARY;
    }

    override string toString() const {
        return "Unary operator node." ~ _operator.toString();
    }
}

class LeafNode : Node {
    Token _token;

    this(Token t) {
        _token = t;
        _nodeType = NodeType.LEAF;
    }


    override string toString() const {
        return "Leaf node." ~ _token.toString();
    }
}

