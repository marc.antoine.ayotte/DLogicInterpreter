import Lexer;
import Parser;
import std.stdio;
import std.conv;
import std.range;
import std.algorithm;

class Interpreter
{
    Node _root;

    this(Node node) {
        _root = node;
    }

    bool[] evaluate(){
        return evaluate(_root);
    }

    bool[] evaluate(Node node) {
        switch(node._nodeType){
            case NodeType.BINARY: {
                BinNode bnode = to!BinNode(node);
                return calculate(bnode._left, bnode._right, bnode._operator);
            }
            break;
            case NodeType.UNARY : {
                UnNode unode = to!UnNode(node);
                return calculate(unode);
            }
            break;
            case NodeType.LEAF : {
                LeafNode lnode = to!LeafNode(node);
                assert(lnode._token.type == TokenType.PROPOSITION);
                return (cast(Proposition)lnode._token)._truthValues;

            }
            break;
            default: assert(false);
        }
    }



    bool[] calculate(Node left, Node right, Token op) {
        bool[] leftValues = evaluate(left);
        bool[] rightValues = evaluate(right);
        switch(op.type) {
            case TokenType.AND: 
                return zip(leftValues, rightValues).map!(x => x[0] && x[1]).array;
            case TokenType.OR:
                return zip(leftValues, rightValues).map!(x => x[0] || x[1]).array;
            case TokenType.IMPLY:
                return zip(leftValues, rightValues).map!(x => !x[0] || x[1]).array;
            case TokenType.EQUALS:
                return zip(leftValues, rightValues).map!(x => x[0] == x[1]).array;
            default: assert(false);
        }
    }

    bool[] calculate(UnNode node) {
        if(node._operator.type == TokenType.NEGATION) {
            return evaluate(node._next).map!(x => !x).array;
        }
        assert(false);
    }
}
