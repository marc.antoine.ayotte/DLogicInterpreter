/************************************************************************
* Program written by Marc-Antoine Ayotte. August 2016.                  *
* This is only a learning exercise to better understand interpreters.   *
* I followed Ruslan Spivak's tutorial(which can be found here           *
* https://ruslanspivak.com/lsbasi-part1/) and made this as an exercise. *
* Any problems, errors, mistakes in this program are my own.            *
* Use at your own risk.                                                 *
************************************************************************/

import Lexer;
import Parser;
import Interpreter;
import std.stdio;

void main() {
    while(true) {
        writeln("Saisissez l'expression: ");
        string input = readln();
        Lexer lexer = new Lexer(input);
        Parser parser = new Parser(lexer);
        auto tree = parser.parse();
        Interpreter interpreter = new Interpreter(tree);
        auto result = interpreter.evaluate();
        writeln(formatResult(result));
    }
}

string formatResult(bool[] result){
    string format = "\n********\n";
    bool tautology = true;
        foreach(r; result) {
            if(r) format ~= "* VRAI *\n";
            else format ~= "* FAUX *\n";
            tautology &= r;
        }
        format ~="********\n";
        if(tautology) format ~= "Ceci est une tautologie.\n";
        else format ~= "Ceci n'est pas une tautologie.\n";
        return format;
}