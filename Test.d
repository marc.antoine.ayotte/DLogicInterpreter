import Lexer;
import Parser;
import Interpreter;
import std.stdio;

void main() {
    string[] inputs;
    inputs ~= "~~P|~~~P"; //T
    inputs ~= "P|~P"; //T
    inputs ~= "(P>Q)>(~Q>~P)";//T
    inputs ~= "(P>Q)=(~Q>~P)";//T
    inputs ~= "(P>Q)=(~P|Q)";//T
    inputs ~= "~(P&Q) = (~P|~Q)";//T
    inputs ~= "~(P|Q) = (~P&~Q)";//T
    inputs ~= "P|Q";//F
    inputs ~= "P&~P";//F
    inputs ~= "P";//F
    bool[]  testResults;

    foreach(input;inputs) {
        Lexer lexer = new Lexer(input);
        Parser parser = new Parser(lexer);
        auto tree = parser.parse();
        Interpreter interpreter = new Interpreter(tree);
        auto result = interpreter.evaluate();
        bool tautology= true;
        foreach(r; result) {
            tautology &= r;
        }
        testResults ~= tautology;
    }
    assert(testResults[0]);
    assert(testResults[1]);
    assert(testResults[2]);
    assert(testResults[3]);
    assert(testResults[4]);
    assert(testResults[5]);
    assert(testResults[6]);
    assert(!testResults[7]);
    assert(!testResults[8]);
    assert(!testResults[9]);

    writeln("All tests worked.");
}