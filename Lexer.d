import std.stdio;
import std.conv;
import std.math;
import std.container.rbtree;

enum TokenType { NEGATION, AND, OR, IMPLY, EQUALS, LPAREN, RPAREN, PROPOSITION, EOF }
const char[33] grammar = ['&','(',')','=', '>','A','B','C','D','E','F','G','H','I','J',
                          'K','L','M','N','O','P','Q','R','S','T','U','V','W','X',
                          'Y','Z','|','~'];

class Lexer {
    string _input;
    Token[] _tokens;
    int _index = 0;
    RedBlackTree!int propositions;
    bool[][char] truthTables;

    this(string input) {
        propositions = new RedBlackTree!int();
        _input = input;
        _tokens = tokenize(input);
        assignTruthTables();
    }

    Token[] tokenize(string input) {
        Token[] tokens;
        foreach(char c; input) {
            if(inGrammar(c)){
                TokenType type = getType(c);
                Token token;
                if(type == TokenType.PROPOSITION){
                    propositions.insert(cast(int)c);
                    token = new Proposition(type, c);
                }
                else {
                    token = new Token(type, c);
                }
                tokens ~= token;
            }
        }
        return tokens;
    }

    void assignTruthTables() {
        //Creates truth tables with the
        real n = cast(real)propositions.length;
        int combinations = cast(int)exp2(n);
        n -= 1;
        int alter = cast(int)exp2(n); 
        auto iter = propositions[];
        foreach (asciiCode; iter) {
            char prop = cast(char)asciiCode;
            bool truthValue = false;
            bool[] table;
            for(int i = 0; i < combinations; ++i) {
                if(i % alter == 0) truthValue = !truthValue;
                table ~= truthValue;
            }
            truthTables[prop] = table;
            n -= 1;
            alter = cast(int)exp2(n);
        }

        //Assign truth tables to the right tokens
        foreach(token; tokens) {
            if(token.type == TokenType.PROPOSITION)
                (cast(Proposition)token)._truthValues = truthTables[token.value];
        }
    }

    bool inGrammar(char c) {
        int left = 0;
        int right = grammar.length - 1;
        if (left > right) return false;
        //Binary search in grammar for character.
        while(left <= right) {
            int middle = cast(int)floor( (left + right) / 2.0 );
            int searchedChar = cast(int)c;
            int currentChar = cast(int)grammar[middle]; 
            if(searchedChar == currentChar ){
                return true;
            }
            else if (searchedChar < currentChar) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }
        
        return false;
    }

    TokenType getType(char c) {
        switch(c) {
                case '~': return TokenType.NEGATION;
                case '&': return TokenType.AND;
                case '|': return TokenType.OR;
                case '>': return TokenType.IMPLY;
                case '=': return TokenType.EQUALS;
                case '(': return TokenType.LPAREN;
                case ')': return TokenType.RPAREN;
                default : return TokenType.PROPOSITION;
        }
        assert(false);
    }

        Token getNextToken() {
        if(_index < _tokens.length) 
            return _tokens[_index++];
        return new Token(TokenType.EOF, '/');
    }

    @property Token[] tokens() {
        return _tokens;
    }
}

class Token {
    TokenType _type;
    char _value;

    this(TokenType t, char v) {
        _type = t;
        _value = v;
    }

    @property TokenType type() {
        return _type;
    }

    @property char value() {
        return _value;
    }

    override string toString() const {
        string typeText, valueText;
        valueText = to!string(_value);
        switch(_type) {
            case TokenType.NEGATION: typeText = "NEGATION";
                break;
            case TokenType.AND: typeText = "AND";
                break;
            case TokenType.OR: typeText = "OR";
                break;
            case TokenType.IMPLY: typeText = "IMPLY";
                break;
            case TokenType.EQUALS: typeText = "EQUALS";
                break;
            case TokenType.LPAREN: typeText = "LPAREN";
                break;
            case TokenType.RPAREN: typeText = "RPAREN";
                break;
            case TokenType.PROPOSITION: typeText = propTypeText(this);
                break;
            default: assert(false);
                break;
        }
        return "Token value: " ~ valueText ~ "  type: " ~ typeText;
    }

    string propTypeText(const Token token) const {
        Proposition prop = cast(Proposition)token;
        string typeText = "PROPOSITION\n";
        foreach(truthValue; prop._truthValues) {
            if(truthValue) typeText ~= "V";
            else typeText ~= "F";
            typeText ~= "\n";
        }
        return typeText;
    }
}

class Proposition : Token {

    bool[] _truthValues;

    this(TokenType t, char v) {
        super(t,v);
    }


}