Programme permettant l'interprétation d'expressions de la logique classique.

## Installation :

Deux options s'offrent à vous :

 1. Faire une copie du code source et le compiler vous-même à l'aide d'un 
[compilateur du langage D.](http://dlang.org/download.html)   
 2. Télécharger le programme [InterpreteurLogique.exe](https://gitlab.com/Vladivostof/DLogicInterpreter/blob/master/InterpreteurLogique.exe)  

## Utilisation:

Pour démarrer le programme, cliquez deux fois dessus ou ouvrez votre console (Run > cmd) et aller jusqu'au dossier où se trouve le programme. (cd chemin/jusquau/fichier). Vous pouvez ensuite simplement saisir InterpreteurLogique et le programme va démarrer.

Vous pouvez ensuite saisir une à une les expressions et le programme vous donnera une table de vérité représentant le résultat et vous dira s'il s'agit d'une tautologie ou non.

## Syntaxe

Symboles:

 * Négation : ~  
 * Conjonction (et) : &  
 * Disjonction (ou) : |  
 * Implication : >  
 * Équivalence : =  
 * Propositions : n'importe quelle lettre de l'alphabet en MAJUSCULE  

Notez que les expressions sont associatives à gauche, et que l'ordre des opérations est :

 1. (,)  
 2. ~  
 3. >, =  
 4. &, |  

Exemples :
`P>Q>R` équivaut à `((P>Q)>R)`  
` P & P > Q > Q` équivaut à ` P & ((P > Q) > Q)`  
Il faut donc écrire ` (P & (P > Q)) > Q` pour la tautologie.

## Autres

Si le programme plante et affiche un message semblable à `core.exception.AssertError@Parser.d(#): Assertion failure` c'est que vous avez fourni une expression mal formée. Si vous pensez qu'il n'y a pas d'erreur dans votre expression, vous pouvez me contacter en créant une Issue.

